var usrname = document.getElementById("usrname");
var psw = document.getElementById("psw");
var cpsw = document.getElementById("cpsw");
var matchpwd = document.getElementById("matchpwd");
var email = document.getElementById("email");
var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
var cEmail = document.getElementById("cEmail");
var length = document.getElementById("length");
let vusrname="";
let vpsw="";
let vemail="";
let vcpsw="";
let vcemail="";

// username
usrname.onfocus = function () {
  document.getElementById("unmessage").style.display = "block";
}
usrname.onblur = function () {
  document.getElementById("unmessage").style.display = "none";
}
usrname.onkeyup = function () {
  // Validate length
  if (usrname.value.length >= 3) {
    unval.classList.remove("invalid");
    unval.classList.add("valid");
    vusrname=true;
  } else {
    unval.classList.remove("valid");
    unval.classList.add("invalid");
    vusrname=false;
  }
}

// password 
psw.onfocus = function () {
  document.getElementById("pswmessage").style.display = "block";
}
psw.onblur = function () {
  document.getElementById("pswmessage").style.display = "none";
}
psw.onkeyup = function () {
  if (psw.value.length >= 6) {
    length.classList.remove("invalid");
    length.classList.add("valid");
    vpsw=true;
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
    vpsw=false;
  }
}

// confirm password
cpsw.onfocus = function () {
  document.getElementById("cpswmessage").style.display = "block";
}
cpsw.onblur = function () {
  document.getElementById("cpswmessage").style.display = "none";
}
cpsw.onkeyup = function () {
  if (psw.value == cpsw.value) {
    matchpwd.classList.remove("invalid");
    matchpwd.classList.add("valid");
    vcpsw=true;
  } else {
    matchpwd.classList.remove("valid");
    matchpwd.classList.add("invalid");
    vcpsw=false;
  }
}

// email
email.onfocus = function () {
  document.getElementById("emailmessage").style.display = "block";
}
email.onblur = function () {
  document.getElementById("emailmessage").style.display = "none";
}
email.onkeyup = function () {
  if (email.value.match(mailformat)) {
    emailvalid.classList.remove("invalid");
    emailvalid.classList.add("valid");
    vemail=true;
  } else {
    emailvalid.classList.remove("valid");
    emailvalid.classList.add("invalid");
    vemail=false;
  }
}

// confirm email
cEmail.onfocus = function () {
  document.getElementById("cemailmessage").style.display = "block";
}
cEmail.onblur = function () {
  document.getElementById("cemailmessage").style.display = "none";
}
cEmail.onkeyup = function () {
  if (email.value == cEmail.value) {
    cemailvalid.classList.remove("invalid");
    cemailvalid.classList.add("valid");
    vcemail=true;

  } else {
    cemailvalid.classList.remove("valid");
    cemailvalid.classList.add("invalid");
    vcemail=false;
  }
}


function onChange() {
  if (psw.value === cpsw.value) {
    cpsw.setCustomValidity('');
  } else {
    cpsw.setCustomValidity('Passwords do not match');
  }
}

function eonChange() {
  if (email.value === cEmail.value) {
    cEmail.setCustomValidity('');
  } else {
    cEmail.setCustomValidity('email do not match');
  }
}

function ok(){
  if(vusrname && vemail && vpsw && vcemail && vcpsw){
  alert("berhasil")}
  else {alert ("validasi lagi!")}
  // return "
}

